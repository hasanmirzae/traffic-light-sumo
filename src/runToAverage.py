import runner
import xml.etree.ElementTree as ET
import numpy as np
import subprocess
import sys

def getAverage():
    tree = ET.parse('../output/tripinfo-output.xml')
    root = tree.getroot()
    arr  = [int(tag.attrib['waitSteps']) for tag in root]
    return np.average(arr)
    

class options:
    nogui = True
    
    
def run():
    sumoProcess = subprocess.Popen(["python3.5","runner.py","--nogui"], stdout=sys.stdout, stderr=sys.stderr)
    sumoProcess.wait()    
    

for i in range(0,1):
    print("Start simulation[{}]:".format(i))
    run()
    print("Simulation[{}] end. Wait Time Average = {}".format(i,getAverage()))
