#!/usr/bin/env python
"""
@file    runner.py
@author  Lena Kalleske
@author  Daniel Krajzewicz
@author  Michael Behrisch
@author  Jakob Erdmann
@date    2009-03-26
@version $Id: runner.py 20433 2016-04-13 08:00:14Z behrisch $

Tutorial for traffic light control via the TraCI interface.

SUMO, Simulation of Urban MObility; see http://sumo.dlr.de/
Copyright (C) 2009-2016 DLR/TS, Germany

This file is part of SUMO.
SUMO is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.
"""
from __future__ import absolute_import
from __future__ import print_function

import os
import sys
import optparse
import subprocess
from smtpd import program
from macpath import curdir
from _ast import Try
from symbol import except_clause
from traci._trafficlights import Phase
# we need to import python modules from the $SUMO_HOME/tools directory
try:
    sys.path.append(os.path.join(os.environ.get("SUMO_HOME"), "tools"))  # tutorial in docs

    from sumolib import checkBinary
except ImportError:
    sys.exit(
        "please declare environment variable 'SUMO_HOME' as the root directory of your sumo installation (it should contain folders 'bin', 'tools' and 'docs')")
import traci
from traci import constants as tc

# the port used for communicating with your sumo instance
PORT = 8874
IN_EDGES_IDS = ["1i","2i","3i","4i"]
IN_LANES_IDS = []
IN_LANES_LENGTH = {}
MIN_GREEN_TIME = 30 # seconds
MAX_RED_TIME = 120 # seconds
TRANSITION_TIME = 6 # seconds

laneToSignalIndex = {}
priority = {}
friendOf = {}
linksOf = {}
time = {"G":0,"y":0}



def updatePhases(program,group):
    mainPhaseState = ""
    transState = ""
    currState = program._phases[0]._phaseDef
    for i in range(0,len(IN_LANES_IDS)):
        if IN_LANES_IDS[i] in group:
            mainPhaseState += 'G'
            transState += currState[i]
        elif currState[i] == 'G':
            mainPhaseState += 'r'
            transState += 'y'
        else:
            mainPhaseState += 'r'
            transState += 'r'
    program._phases = []
    mainPhaseDuration = MIN_GREEN_TIME * 1000
    transitionTime = TRANSITION_TIME * 1000
        
    program._phases.append(Phase(mainPhaseDuration,mainPhaseDuration,mainPhaseDuration,mainPhaseState))
    program._phases.append(Phase(transitionTime,transitionTime,transitionTime,transState))
    traci.trafficlights.setCompleteRedYellowGreenDefinition('0',program)
    setPhase(0)
    
def endTransitPhase(program):
    currState = program._phases[0]._phaseDef
    state = currState.replace('y','r')
    phases = program._phases
    p = phases[0]
    p._duration = (MIN_GREEN_TIME - TRANSITION_TIME)*1000
    p._duration1 = (MIN_GREEN_TIME - TRANSITION_TIME)*1000
    p._duration2 = (MIN_GREEN_TIME - TRANSITION_TIME)*1000
    p._phaseDef = state
    traci.trafficlights.setCompleteRedYellowGreenDefinition('0',program)
    
    
    



def getProgram(tlsID,programId):
    for p in traci.trafficlights.getCompleteRedYellowGreenDefinition(tlsID):
        if programId == p._subID:
            return p
    return None

def rank(lID,queues):
    return priority[lID]*queues[lID]


def findLeader(queues):
    leader = IN_LANES_IDS[0]
    for id in set(IN_LANES_IDS):
        if rank(id,queues) > rank(leader,queues):
            leader = id 
    return leader
            
def areFriends(lID1,lID2):
    try:
        return lID2 in friendOf[lID1] or lID1 in friendOf[lID2]
    except ValueError:
        return False
def isFriendOfGroup(lID,group):
    for id in group:
        if not areFriends(lID, id):
            return False
    return True    
            
def createGroup(leaderId):
    group = []
    for id in set(IN_LANES_IDS):
        if areFriends(leaderId,id) and isFriendOfGroup(id, group):
            group.append(id)
    return group

def updatePriorities(group):
    for id in set(IN_LANES_IDS):
        if id in group:
            priority[id] = 1
        else:
            if (priority[id]+1)*MIN_GREEN_TIME > MAX_RED_TIME:
                priority[id] += 1000 # just a big number to insure that it will get green signal if there is at least one car
            else:
                priority[id] += 1

def updateProgram():
    global currentGroup
    if exitsAtLeastOneCar():
        program = getProgram("0","my_program")
        leaderId = findLeader(queues)
        currentGroup = createGroup(leaderId)
        updatePriorities(currentGroup)
        updatePhases(program,currentGroup)
    
    


       
        
def _numberCarsInViewZone(lID,viewLength):
    nb = 0
    # [x for x in my_list if x.attribute == value]
    for vID in traci.lane.getLastStepVehicleIDs(lID):
        if traci.vehicle.getLanePosition(vID) >= IN_LANES_LENGTH[lID] - viewLength:
             nb += 1
    return nb

def setIncomLanesLengthConstant():
    for lID in set(IN_LANES_IDS):
        IN_LANES_LENGTH[lID] = traci.lane.getLength(lID) 
        
        
def countCarsInViewZoneOnStep(viewLength):
    return dict(map(lambda id : (id,_numberCarsInViewZone(id, viewLength)),set(IN_LANES_IDS)))        


def setIncomLanesIdsConstant():
    # !!! there might be doublication, but we need them
    IN_LANES_IDS.extend(traci.trafficlights.getControlledLanes("0"))
    
def initPriority():
    for id in set(IN_LANES_IDS):
        priority[id] = 1    
        
def initFriendshipDict():
    friendOf['1i_0'] = ['1i_0','1i_1','1i_2','2i_0','2i_1','2i_2']        
    friendOf['1i_1'] = ['1i_0','1i_1','1i_2','2i_0','2i_1','2i_2']        
    friendOf['1i_2'] = ['1i_0','1i_1','1i_2']        
    friendOf['2i_0'] = ['2i_0','2i_1','2i_2','1i_0','1i_1']        
    friendOf['2i_1'] = ['2i_0','2i_1','2i_2','1i_0','1i_1']        
    friendOf['2i_2'] = ['2i_0','2i_1','2i_2','1i_0','1i_1']
    friendOf['3i_0'] = ['3i_0','3i_1','3i_2','4i_0','4i_1','4i_2']        
    friendOf['3i_1'] = ['3i_0','3i_1','3i_2','4i_0','4i_1','4i_2']        
    friendOf['3i_2'] = ['3i_0','3i_1','3i_2','4i_0','4i_1','4i_2']
    friendOf['4i_0'] = ['4i_0','4i_1','4i_2','3i_0','3i_1','3i_2']        
    friendOf['4i_1'] = ['4i_0','4i_1','4i_2','3i_0','3i_1','3i_2']
    friendOf['4i_2'] = ['4i_0','4i_1','4i_2','3i_0','3i_1','3i_2']        
        

def setLinksOfLanes():
    #create a dict, mapping lane id -> connecting outgoing lanes
    for id in set(IN_LANES_IDS):
        linksOf[id] = []
        # t- tuple, t[0] - lane id
        for t  in traci.lane.getLinks(id):
            linksOf[id].append(t[0])
            
                
            
currentGroup = []# current group of lanes with green signal
lastPhase = 1 
queues = {} # real time queues length

def exitsAtLeastOneCarInCurrentGroup():
    for lID in currentGroup:
        if queues[lID] > 0:
            return True
    return False

def existsAtLeastOneCarOutOfGroup():
    for lID in set(IN_LANES_IDS):
        if (not lID in currentGroup) and queues[lID] > 0:
            return True
    return False

def exitsAtLeastOneCar():
    for lID in set(IN_LANES_IDS):
        if queues[lID] > 0:
            return True
    return False
    
def getPhase():
    return traci.trafficlights.getPhase("0")

def setPhase(index):
    traci.trafficlights.setPhase("0",index)


def updateTrafficLightOnStep():
    global lastPhase,queues
    queues = countCarsInViewZoneOnStep(100)
    endOfCycle = getPhase() == 0 and lastPhase > 0
    noCarsInCurrentGroup = not exitsAtLeastOneCarInCurrentGroup()
    if endOfCycle:
        updateProgram()
    elif noCarsInCurrentGroup and getPhase() == 0 and existsAtLeastOneCarOutOfGroup():
        setPhase(1) # switch to transition phase to reprogram
    lastPhase = getPhase()
        

def run():
    """execute the TraCI control loop"""
    traci.init(PORT)
    traci.trafficlights.setProgram("0","my_program")
    setIncomLanesIdsConstant();
    setIncomLanesLengthConstant()
    initPriority()
    initFriendshipDict()
    setLinksOfLanes()
    step = 0
    global queues
    queues = countCarsInViewZoneOnStep(100)
    updateProgram()
    while traci.simulation.getMinExpectedNumber() > 0:
        traci.simulationStep()
        updateTrafficLightOnStep()
        step += 1
    traci.close()
    sys.stdout.flush()


def get_options():
    optParser = optparse.OptionParser()
    optParser.add_option("--nogui", action="store_true",
                         default=False, help="run the commandline version of sumo")
    options, args = optParser.parse_args()
    return options


def main(options):
    # this script has been called from the command line. It will start sumo as a
    # server, then connect and run
    if options.nogui:
        sumoBinary = checkBinary('sumo')
    else:
        sumoBinary = checkBinary('sumo-gui')

    # generate cross.net.xml
    p = subprocess.Popen(['netconvert','data/cross.netccfg'])
    p.communicate()


    # this is the normal way of using traci. sumo is started as a
    # subprocess and then the python script connects and runs
    sumoProcess = subprocess.Popen([sumoBinary, "-c", "data/cross.sumocfg", 
                                    "--tripinfo-output", "../output/tripinfo-output.xml",
                                    "--remote-port", str(PORT),
                                    "--duration-log.statistics"
                                    ], stdout=sys.stdout, stderr=sys.stderr)
    run()
    sumoProcess.wait()    

# this is the main entry point of this script
if __name__ == "__main__":
    options = get_options()
    main(options)

