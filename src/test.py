import xml.etree.ElementTree as ET
import numpy as np
tree = ET.parse('../output/tripinfo-output.xml')
root = tree.getroot()
arr  = [int(tag.attrib['waitSteps']) for tag in root]
print(arr)
print(np.average(arr))